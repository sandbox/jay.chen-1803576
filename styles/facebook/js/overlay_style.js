/**
 * @file
 * Overlay style javascript of Facebook style.
 */

(function ($) {
  Drupal.behaviors.overlayStyle = {
    attach: function(context, settings) {
      //settings.overlayChild.redirect = 'http://google.com';
      //settings.overlayChild.refreshPage = true;
    }
  };
  //Drupal.overlayChild.behaviors.overlayStyle  = function (context, settings) {
    //settings.overlayChild.redirect = 'http://google.com';
    //parent.Drupal.overlay.refreshPage = true;
  //};
})(jQuery);

(function ($) {
  // Adjust the overlay dimensions.
  Drupal.behaviors.overlayStyle = {
    attach: function (context) {
      console.log(Drupal.settings.overlayStyle.q.substring(0, 5));
      if (Drupal.settings.overlayStyle.q.substring(0, 5) == 'admin') {
        var overlayWidth = '800px';
      }
      else if (Drupal.settings.overlayStyle.q == 'node/add') {
        var overlayWidth = '400px';
      }
      else if (Drupal.settings.overlayStyle.q.substring(0, 8) == 'node/add') {
        var overlayWidth = '1000px';
      }
      else {
        var overlayWidth = '800px';
      }

      $('#overlay', context).each(function() {
        $(this).css({
          opacity: 0
          //'overflow' : 'hidden',
          //width     : '0px',
          //height : '0px'
        })
        .animate({
          opacity: 0.2,
          width: 'easeInBounce',
          height: 'easeInBounce',
          //width: 0,
          //height: 0,
          top: '0px'
          }, 100, function() {
          // Animation complete.
          }
        )
        .animate({
            opacity: 0.4,
            width: 'easeInBounce',
            height: 'easeInBounce',
            //width: 0,
            //height: 0,
            top: '100px'
          }, 100, function() {
            // Animation complete.

         })
        //.slideDown()
        .animate({
            opacity: 0.6,
            width: 'easeOutBounce',
            height: 'easeOutBounce',
            //width: 'auto',
            //height: 'auto',
            top: '0px'
          }, 100, function() {
            // Animation complete.

          })
        .animate({
            opacity: 1,
            width: 'easeOutBounce',
            height: 'easeOutBounce',
            width: overlayWidth,
            height: 'auto',
            top: '50px'
          }, 500, function() {
            // Animation complete.

          });
      });
    }
  };
})(jQuery);
