/**
 * @file
 * Overlay style javascript of default style.
 */

(function ($) {

  Drupal.behaviors.overlayStyle = {
    attach: function(context, settings) {
      //settings.overlayChild.redirect = 'http://google.com';
      //settings.overlayChild.refreshPage = true;

    }
  };

  //Drupal.overlayChild.behaviors.overlayStyle  = function (context, settings) {
    //settings.overlayChild.redirect = 'http://google.com';
    //parent.Drupal.overlay.refreshPage = true;

  //};
})(jQuery);

(function ($) {

// Adjust the overlay dimensions.
Drupal.behaviors.myModule = {

  attach: function (context) {

    //alert($(this).width());
    $('#overlay', context).each(function() {
      //alert($(this).width());
      $(this).css({
        opacity: 0
          //'overflow' : 'hidden',
          //width     : '0px',
          //height : '0px'
        })
        .animate({
            opacity: 0.2,
            width: 'easeInBounce',
            height: 'easeInBounce',
            width: 0,
            height: 0,
            top: '0px'
          }, 100, function() {
            // Animation complete.
         })
        .animate({
            opacity: 0.4,
            width: 'easeInBounce',
            height: 'easeInBounce',
            width: 0,
            height: 0,
            top: '100px'
          }, 100, function() {
            // Animation complete.
         })
        //.slideDown()
        .animate({
            opacity: 0.6,
            width: 'easeOutBounce',
            height: 'easeOutBounce',
            //width: 'auto',
            //height: 'auto',
            top: '0px'
          }, 100, function() {
            // Animation complete.
          })
        .animate({
            opacity: 1,
            width: 'easeOutBounce',
            height: 'easeOutBounce',
            //width: 'auto',
            //height: 'auto',
            top: '50px'
          }, 100, function() {
            // Animation complete.
          });
    });
  }
};

})(jQuery);
