<?php

/**
 * @file
 * Overlay style settings.
 */

/**
 * Menu callback: displays the overlay style settings page.
 */
function overlay_style_settings() {
  $all_styles = overlay_style_get_styles();
  $styles = array();
  $styles['none'] = t('None');
  foreach ($all_styles as $key => $style) {
    $styles[$key] = $style['title'];
  }

  $form['overlay_style_current_style'] = array(
    '#required' => TRUE,
    '#type' => 'select',
    '#title' => t('Choose a style'),
    '#options' => $styles,
    '#default_value' => variable_get('overlay_style_current_style', 'default'),
    '#description' => '',
  );
  return system_settings_form($form);
}
