<?php

/**
 * @file
 * Hooks provided by overlay style module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define overlay styles.
 */
function hook_overlay_style() {
  $path = drupal_get_path('module', 'overlay_style');
  return array(
    'default' => array(
      'title' => t('Default'),
      'class' => array('overlay-style-default'),
      'css' => array(
        $path . '/styles/default/css/overlay_style.css',
      ),
      'js' => array(
        $path . '/styles/default/js/overlay_style.js',
      ),
      'template' => array(
        'name' => 'overlay_style_default',
        'path' => $path . '/styles/default/tpl',
        'template' => 'overlay-style-default',
      ),
      'overlay_theme' => '',
    ),
    'facebook' => array(
      'title' => t('Facebook'),
      'class' => array('overlay-style-facebook'),
      'css' => array(
        $path . '/styles/facebook/css/overlay_style.css',
      ),
      'js' => array(
        $path . '/styles/facebook/js/overlay_style.js',
      ),
      'template' => array(
        'name' => 'overlay_style_facebook',
        'path' => $path . '/styles/facebook/tpl',
        'template' => 'overlay-style-facebook',
      ),
      'overlay_theme' => 'bartik',
    ),
  );
}

/**
 * @} End of "addtogroup hooks".
 */
